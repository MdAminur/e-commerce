const crypto = require('crypto');
const util = require('util');
const Repository = require('./repository');

const scryprt = util.promisify(crypto.scrypt);

class UserRepository extends Repository{
    async create(userRequest) {
        userRequest.id = this.generateRandomId();
        const userList = await this.getAll();
        const salt = crypto.randomBytes(8).toString('hex');
        const hashedPassword = await scryprt(userRequest.password, salt, 64);
        const user = {
            ...userRequest,
            password: `${hashedPassword.toString('hex')}.${salt}`
        }
        userList.push(user);
        await this.writeAll(userList);
        return user;
    }

    async comparePasswords(savedPass, requestPass) {
        const [hashSavedPassword, saltSavedPassword] = savedPass.split('.');
        const hashRequestPassword = (await scryprt(requestPass, saltSavedPassword, 64)).toString('hex');

        return hashSavedPassword === hashRequestPassword;
    }

}

module.exports = new UserRepository('users.json')

