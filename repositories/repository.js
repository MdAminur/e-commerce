const fs = require("fs");
const crypto = require("crypto");
module.exports = class Repository {

    constructor(filename) {
        if (!filename) {
            throw new Error('Creating a repository requires a filename');
        }
        this.filename = filename;

        try {
            fs.accessSync(this.filename);
        } catch (err) {
            fs.writeFileSync(this.filename, '[]')
        }
    }

    async create(attributes) {
        attributes.id = this.generateRandomId();
        const records = await this.getAll();
        records.push(attributes);
        await this.writeAll(records);
        return attributes;
    }

    generateRandomId() {
        return crypto.randomBytes(4).toString('hex');
    }

    async writeAll(data) {
        await fs.promises.writeFile(
            this.filename,
            JSON.stringify(data, null, 2)
        );
    }

    async getAll() {
        return JSON.parse(await fs.promises.readFile(this.filename, {encoding: 'utf-8'}));
    }

    async getOne(id) {
        const records = await this.getAll();
        return records.find((record) => record.id === id);
    }

    async update(id, updateData) {
        const records = await this.getAll();
        const record = records.find((record) => record.id === id);

        if (!record) {
            throw new Error(`No Data found with id ${id}`)
        }

        Object.assign(record, updateData);
        await this.writeAll(records);
    }

    async delete(id) {
        const records = await this.getAll();
        const filteredRecords = records.filter((data) => data.id !== id);
        await this.writeAll(filteredRecords);
    }

    async getOneBy(filters) {
        const records = await this.getAll();
        for (let record of records) {
            for (let key in record) {
                if (record[key] === filters[key]) {
                    return record;
                }
            }
        }
    }
}