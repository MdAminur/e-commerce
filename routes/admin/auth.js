const express = require('express');
const {validationResult} = require('express-validator');
const userRepository = require('../../repositories/userRepository');
const signupTemplate = require('../../views/admin/auth/signup');
const signinTemplate = require('../../views/admin/auth/signin');
const {requireEmail
    , requirePassword
    , requirePasswordConfirmation
    ,requireEmailExist
    , requirePasswordExist
} = require('./validators');

const router = express.Router();
router.get('/signup', (req, res) => {
    res.send(signupTemplate({ req }));
});

router.post(
    '/signup',
    [requireEmail, requirePassword, requirePasswordConfirmation],
    async (req, res) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.send(signupTemplate({ req, errors }));
        }

        const { email, password, passwordConfirmation } = req.body;
        const user = await userRepository.create({ email, password });
        req.session.userId = user.id;
        res.send('Account created!!!');
    }
);

router.get('/signin', (req, res) => {
    res.send(signinTemplate({}));
});

router.post('/signin',
    [requireEmailExist, requirePasswordExist],
    async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.send(signinTemplate({ errors }));
    }

    const user = await userRepository.getOneBy({email: req.body.email});
    req.session.userId = user.id;
    return res.send('You are signed in');
});

router.get('/signout', (req, res) => {
    req.session = null;
    res.send('You are logged out')
})



module.exports = router;
