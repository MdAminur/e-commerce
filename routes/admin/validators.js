const { check } = require('express-validator');
const usersRepository = require('../../repositories/userRepository');

module.exports = {
    requireEmail: check('email')
        .trim()
        .normalizeEmail()
        .isEmail()
        .withMessage('Must be a valid email')
        .custom(async email => {
            const existingUser = await usersRepository.getOneBy({ email });
            if (existingUser) {
                throw new Error('Email in use');
            }
        }),
    requirePassword: check('password')
        .trim()
        .isLength({ min: 4, max: 20 })
        .withMessage('Must be between 4 and 20 characters'),
    requirePasswordConfirmation: check('passwordConfirm')
        .trim()
        .isLength({ min: 4, max: 20 })
        .withMessage('Must be between 4 and 20 characters')
        .custom((passwordConfirm, { req }) => {
            if (passwordConfirm !== req.body.password) {
                throw new Error('Passwords must match');
            }

            return true;
        }),
    requireEmailExist: check('email')
        .trim()
        .normalizeEmail()
        .isEmail()
        .custom(async email => {
            const user = await usersRepository.getOneBy({email});
            if (!user){
                throw new Error('No User found');
            }
        }),
    requirePasswordExist: check('password')
        .trim()
        .custom(async (password, {req}) => {
            const user = await usersRepository.getOneBy({email: req.body.email});

            if (!user){
                throw new Error('Invalid Password');
            }

            const validPassword = await usersRepository.comparePasswords(user.password, password);
            if (!validPassword) {
                throw new Error('Invalid Password');
            }

        })
};
